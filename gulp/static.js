'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')();

gulp.task('move', function () {
  return gulp.src(path.join(conf.paths.src, '**/*.html'))
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve')));
});
