'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');

function isOnlyChange(event) {
  return event.type === 'changed';
}

gulp.task('watch', function () {

  gulp.watch([
    path.join(conf.paths.src, '/app/**/*.js'),
    path.join(conf.paths.src, '/app/**/*.ts')
  ], function(event) {
    if(isOnlyChange(event)) {
      gulp.start('scripts');
    } 
  });

  gulp.watch(path.join(conf.paths.src, '/**/*.html'), function(event) {
    gulp.start('move');
    browserSync.reload(event.path);
  });
});
