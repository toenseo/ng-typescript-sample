/**
 * The main gulp build file, just define tasks here.
 */

'use strict';

var conf = require('./conf');
var path = require('path');
var gulp = require('gulp');
var del = require('del');

gulp.task('clean', ['tsd:purge'], function (done) {
  del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')], done);
});

gulp.task('build', ['scripts', 'libs']);