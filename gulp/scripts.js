'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');

var plugins = require('gulp-load-plugins')();

var tsProject = plugins.typescript.createProject({
  target: 'es5',
  sortOutput: true
});

gulp.task('scripts', ['tsd:install'], function () {
  return gulp.src(path.join(conf.paths.src, '/app/**/*.ts'))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.tslint())
    .pipe(plugins.tslint.report('prose', {emitError: false}))
    .pipe(plugins.typescript(tsProject))
    .pipe(plugins.concat('app.js'))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app')))
    .pipe(browserSync.reload({stream: true}))
    .pipe(plugins.size())
});
