var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
var concat = require('gulp-concat');

var libs = [
	"node_modules/angular/angular.js",
	"node_modules/jquery/dist/jquery.js",
	"node_modules/angular-animate/angular-animate.js",
	"node_modules/angular-cookies/angular-cookies.js",
	"node_modules/angular-resource/angular-resource.js",
	"node_modules/angular-sanitize/angular-sanitize.js",
	"node_modules/angular-touch/angular-touch.js",
	"node_modules/angular-ui-router/build/angular-ui-router.js"
]; 

gulp.task('libs', function() {
  return gulp.src(libs)
    .pipe(concat('libs.js'))
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app')));
});