/// <reference path="../../.tmp/typings/tsd.d.ts" />

module ngTypescriptSample {
    'use strict';

    export class Config {
        /** @ngInject */
        constructor($logProvider:ng.ILogProvider) {
            // enable log
            $logProvider.debugEnabled(true);
        }

    }
}
