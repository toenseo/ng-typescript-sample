(function () {
  'use strict';

  describe('MainController', function () {

    var mainController;

    beforeEach(module('ngTypescriptSample'));

    beforeEach(inject(function ($controller) {
      mainController = $controller('MainController');
    }));

    it('should define a hello property', function () {
      expect(mainController.hello).toBe("Hello World");
    });

    it('should define an active function', function () {
      expect(mainController.activate).toBeDefined();
    });

  });

})();
