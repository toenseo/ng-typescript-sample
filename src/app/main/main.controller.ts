/// <reference path="../../../.tmp/typings/tsd.d.ts" />

module ngTypescriptSample {
    'use strict';

    export class MainController {
        public hello:string;

        /** @ngInject */
        constructor($timeout:ng.ITimeoutService) {
            this.hello = 'Hello World';
            this.activate($timeout);
        }

        activate($timeout:ng.ITimeoutService) {
            var self = this;
            $timeout(function () {
                self.hello = 'Hello Typescript World';
            }, 4000);
        }

    }
}
