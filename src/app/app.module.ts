/// <reference path="../../.tmp/typings/tsd.d.ts" />
/// <reference path="app.route.ts" />
/// <reference path="app.config.ts" />
/// <reference path="main/main.controller.ts" />

module ngTypescriptSample {
  'use strict';

  angular.module('ngTypescriptSample', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router'])
    .config(Config)
    .config(RouterConfig)
    .controller('MainController', MainController)
}