# README #

Sample angular Typescript project.

### Prerequisites ###

* Install [nodejs](https://nodejs.org/en/) (on Mac `brew install nodejs`)
* Install [TypeScript](http://www.typescriptlang.org/): `npm install -g typescript`
* Install [gulp](http://gulpjs.com/): `npm install -g gulp`

### Setup ###

* Clone this repo.
* Run `npm install`.
* Run `gulp serve`.

### Contact ###

* marc.weinberger@me.com
* oliver.toense@blu-pa.com